#include "hardware_manager.h"
#include "movingAvg.h"

enum states {
  PREFLIGHT,
  PW_ASCENT,
  CST_ASCENT,
  DESCENT,
  POSTFLIGHT
};

// Creates an instance of the SensorData struct
SensorData_t sensorData;

// Start in PREFLIGHT
int state = PREFLIGHT;

// Create a moving average to store previous 20 pressure values
movingAvg pressure(20);

void setup() {
  Serial.begin(9600);
  HM_Init(sensorData);
  pressure.begin();
}


bool transitionCondition() {
  return true;
}


SensorData_t logData;
uint32_t accelThresholdTrueSince = millis();
uint32_t touchdownTimer = millis();
uint32_t lastIntervalPressure = 2 * 10000;

// max altitude occurs at min pressure
// Initial pressure is at 10000
int32_t minPressure = 2 * 10000;
uint32_t prevPrint = millis();
uint32_t prevLog = millis();
// Time between prints
int32_t printDelay = 5000;
// Time between logging
int32_t logDelay = 5000;

void loop() {
  
  HM_ReadSensorData(sensorData);
  // The moving average library only uses ints
  // So we multiply our float by 10000 then cast as int
  pressure.reading((uint32_t)(sensorData.pressure*10000));

  uint32_t newPres = pressure.getAvg();
  // The higher it goes the lower the pressure is
  if (state > PREFLIGHT) {
    if (newPres < minPressure) {
      minPressure = newPres;
    }
  }
  // Print out the sensor data
  if (millis() - prevPrint > printDelay) {
    prevPrint = millis();
    Serial.print("State: ");
    Serial.println(state);
    Serial.print("NewPres: ");
    Serial.println(newPres);
    Serial.print("Accz: ");
    Serial.println(sensorData.accel_z);
  }
  // Logs the sensor data
  if (millis() - prevLog > logDelay) {
    prevLog = millis();
    logData.pressure = newPres;
    logData.accel_z = sensorData.accel_z;
    HM_LogDataToSD(logData);
  }
  
  switch(state) {
    case PREFLIGHT:
      // If our accel isn't what we want, reset the timer
      if (!(sensorData.accel_z > 20)) {
        accelThresholdTrueSince = millis();
      }
      // If we get here, our accel has been at the threshold for 300ms
      // So we should transition!
      if (millis() - accelThresholdTrueSince > 300) {
        printDelay = 250;
        state = PW_ASCENT;
        HM_LogTransitionToSD("Transitioned to POWERED ACSCENT!");
      }
      // Code for PREFLIGHT
      break;
    case PW_ASCENT:
      if (sensorData.accel_z == 0) {
        printDelay = 500;
        state = CST_ASCENT;
        HM_LogTransitionToSD("Transitioned to COASTING FLIGHT!");
      }
      // Code for ASCENT
      break;
    case CST_ASCENT:
      // Higher pressure = Going down
      // This means we are past apogee - go to descent
      if (newPres > minPressure) {
        state = DESCENT;
      }
      break;
    case DESCENT:
      // TODO: Implement descent transition to postflight
      if (newPres - lastIntervalPressure > 10) {
        lastIntervalPressure = newPres;
        touchdownTimer = millis();
      }
      if (millis() - touchdownTimer > 3000) {
        state = POSTFLIGHT;
      }
      // Code for DESCENT
      break;
    case POSTFLIGHT:
      printDelay = 5000;
      break;
    default:
      // Shouldn't get here
      break;
  }
  // Delay for a bit
  delay(10);
}

