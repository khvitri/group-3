#include "hardware_manager.h"

// Function stubs for everything in hardware_manager.h (just so the project will compile)
// These will all need to be created to do something!

void HM_Init(SensorData_t& sensorData) {}

void HM_ReadSensorData(SensorData_t& sensorData) {}

void HM_LogDataToSD(SensorData_t& sensorData) {}

void HM_LogTransitionToSD(int stateID) {}
